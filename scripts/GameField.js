'use strict'
export class GameField {
   constructor(info) {
      this._mode = ['X', 'O']
      this._state = [
         [null, null, null],
         [null, null, null],
         [null, null, null]
      ];
      this._info = info;
   }
   // возвращает массив
   getState() {
      return this._state;
   }
   //  меняет значение массива по полученной координате
   _setState(a) {
      this._state = this._state.map((i, index) => {
         if (index == [`${a[0]}` - 1]) {
            i.splice([`${a[1]}` - 1], 1, this._mode[0])
            this._setMode()
         }
         return i
      })
   }
   // меняет значение, какой игрок ходит
   _setMode() {
      this._mode.reverse()
   }

   getMode() {
      return this._mode;
   }

   // прверяет есть ли победитель или остались ли еще пустые клетки на поле (в массиве)
   isOverGame() {
      if (this._getGameFieldStatus(this._state)) {
         return false
      }
      return this._state.reduce((acc, i) => {
         return acc = i.includes(null)
      })
   }

   // Выведем в консоль победителя и покажем на экране результат
   _setWinner() {
      const result = this.getMode().reverse()
      console.log(`Победил ${result[0]}`)
      return (
         this._info.textContent = `Победил ${result[0]}`
      )
   }

   // получает значения координат от игроков
   fieldCellValue() {
      let a;
      let b;
      b = prompt(`${this._mode[0]} Введите незанятую координату строки и столбца числами через тире, "строка-столбец"`,)
      while (b == null || b == '') { // если игрок нажал отмену или подтвердил ввод без координат
         b = prompt('Заполните координаты',)
      }
      a = b.split('-')
      console.log(parseInt(a[0]))
      level_1: while (a[0] < 1 || a[0] > 3 || a[1] < 1 || a[1] > 3) { // проверяет, чтоб введенные координаты были не больше чем столбцов или строк в массиве
         b = prompt('Координаты должны быть не меньше 0 и не больше 3',)
         if (b == null || b == '') continue level_1;
         a = b.split('-')
      }
      level_2: while (this._state[`${a[0]}` - 1][`${a[1]}` - 1] !== null) { // проверяет, чтоб клетка была пустая
         b = prompt('Клетка занята',)
         if (b == null || b == '' || a[0] < 1 || a[0] > 3 || a[1] < 1 || a[1] > 3) continue level_2;
         a = b.split('-')
      }
      this._setState(a)
   }

   _getGameFieldStatus(array) {
      let m = array.length; // Высота массива равна ширине

      for (let i = 0; i < m; ++i) { // Проверяем, чтобы в строке были одинаковые элементы
         let tmp = array[i][0]; // Первый элемент из проверяемой последовательности элементов, которые должны быть равны
         for (let j = 1; j < m; ++j) {
            if (array[i][j] == null || array[i][j] != tmp)
               break;
            if (j === m - 1) {
               this._setWinner()
               return true;
            }
         }
      }
      for (let j = 0; j < m; ++j) { // Проверяем, чтобы в столбце были одинаковые элементы
         let tmp = array[0][j];
         for (let i = 1; i < m; ++i) {
            if (array[i][j] == null || array[i][j] != tmp)
               break;
            if (i === m - 1) {
               this._setWinner()
               return true;
            }
         }
      }
      { // Диагональ верхний левый угол - нижний правый угол
         let tmp = array[0][0];
         for (let i = 1; i < m; ++i) {
            if (array[i][i] == null || array[i][i] != tmp)
               break;
            if (i === m - 1) {
               this._setWinner()
               return true;
            }
         }
      }
      { // Диагональ нижний левый угол - верхний правый угол
         let tmp = array[m - 1][0];
         for (let j = 1; j < m; ++j) {
            if (array[m - j - 1][j] == null || array[m - j - 1][j] != tmp)
               break;
            if (j === m - 1) {
               this._setWinner()
               return true;
            }
         }
      }
      return false;
   }
}

